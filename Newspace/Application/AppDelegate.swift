//
//  AppDelegate.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import Firebase
import UIKit

final class AppDelegate: NSObject, UIApplicationDelegate {
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
    FirebaseApp.configure()
    return true
  }
}
