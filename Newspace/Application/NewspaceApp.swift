//
//  NewspaceApp.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import SwiftUI

@main
struct NewspaceApp: App {
  
  @UIApplicationDelegateAdaptor(AppDelegate.self)
  private var appDelegate
  
  var body: some Scene {
    WindowGroup {
      NavigationView {
        ArticleListView()
        ArticleDetailView()
      }
    }
  }
}
