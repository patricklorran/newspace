//
//  Article.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import Foundation

struct Article: Decodable, Identifiable {
  
  private static let service = SpaceflightNewsService.shared
  
  let id: Int
  let title: String
  let url: URL
  let imageURL: URL?
  let newsSite: String
  let summary: String
  let publishedAt: Date
  let updatedAt: Date
  let featured: Bool
  
  static func articles(index: Int = 0, count: Int = 10) async -> [Article] {
    do {
      return try await service.articles(start: index, limit: count)
    } catch {
      return []
    }
  }
  
  static func article(id: String) async -> Article? {
    do {
      return try await service.article(id: id)
    } catch {
      return nil
    }
  }
}

extension Article {
  
  private static var dateFormatter: DateFormatter {
    let formatter = DateFormatter()
        formatter.timeZone   = .init(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    return
        formatter
  }
  
  static let sample = Article(
             id: 16849,
          title: "Starlink kicks off Asia expansion in search of more subscribers",
            url: .init(string: "https://spacenews.com/starlink-kicks-off-asia-expansion-in-search-of-more-subscribers/")!,
       imageURL: .init(string: "https://spacenews.com/wp-content/uploads/2022/05/Starlink-RV.jpg"),
       newsSite: "SpaceNews",
        summary: "Starlink is deepening its foothold in Japan as SpaceX’s satellite broadband service looks to expand elsewhere in Asia, which will be critical for generating much-needed revenues.",
    publishedAt: dateFormatter.date(from: "2022-10-13T21:00:11.000Z")!,
      updatedAt: dateFormatter.date(from: "2022-10-13T21:00:12.093Z")!,
       featured: false
  )
}
