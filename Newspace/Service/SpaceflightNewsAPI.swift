//
//  SpaceflightNewsAPI.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import Foundation
import Moya

enum SpaceflightNewsAPI: TargetType {
  case articles(start: Int, limit: Int)
  case article(id: String)
  
  var baseURL: URL { .init(string: "https://api.spaceflightnewsapi.net/v3")! }
  
  var path: String {
    switch self {
      case .articles:
        return "/articles"
      case .article(id: let id):
        return "/articles/\(id)"
    }
  }
  
  var method: Moya.Method { .get }
  
  var task: Task {
    switch self {
      case .articles(start: let start, limit: let limit):
        return .requestParameters(parameters: ["_start": start, "_limit": limit], encoding: URLEncoding.queryString)
      case .article(id: _):
        return .requestPlain
    }
  }
  
  var headers: [String : String]? { nil }
}
