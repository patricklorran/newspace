//
//  SpaceflightNewsService.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import Foundation
import Moya

final class SpaceflightNewsService {
  
  private let decoder  = JSONDecoder()
  private let provider = MoyaProvider<SpaceflightNewsAPI>()
  
  static  let shared   = SpaceflightNewsService()
  
  private init() {
    let dateFormatter = DateFormatter()
        dateFormatter.timeZone   = .init(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    decoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  func articles(start: Int = 0, limit: Int = 10) async throws -> [Article] {
    try await withUnsafeThrowingContinuation { continuation in
      provider.request(.articles(start: start, limit: limit)) { result in
        switch result {
          case .success(let response):
            do {
              continuation.resume(returning: try response.map([Article].self, using: self.decoder))
            } catch {
              continuation.resume(throwing: error)
            }
          case .failure(let error):
              continuation.resume(throwing: error)
        }
      }
    }
  }
  
  func article(id: String) async throws -> Article {
    try await withUnsafeThrowingContinuation { continuation in
      provider.request(.article(id: id)) { result in
        switch result {
          case .success(let response):
            do {
              continuation.resume(returning: try response.map(Article.self, using: self.decoder))
            } catch {
              continuation.resume(throwing: error)
            }
          case .failure(let error):
              continuation.resume(throwing: error)
        }
      }
    }
  }
}
