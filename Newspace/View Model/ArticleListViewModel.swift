//
//  ArticleListViewModel.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 18/10/22.
//

import Foundation

@MainActor
final class ArticleListViewModel: ObservableObject {
  
  enum State {
    case loading
    case loaded([Article])
    case error
  }
  
  @Published
  private(set) var state = State.loading
  
  func fetchArticles() {
    Task { state = .loaded(await Article.articles()) }
  }
}
