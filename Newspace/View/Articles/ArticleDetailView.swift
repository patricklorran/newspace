//
//  ArticleDetailView.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import Firebase
import SwiftUI

struct ArticleDetailView: View {
  
  @Environment(\.openURL)
  private var openURL
  
  var article: Article?
  
  var body: some View {
    if let article = article {
      VStack(alignment: .leading, spacing: 8) {
        AsyncImage(url: article.imageURL) { image in
          image
            .resizable()
            .scaledToFill()
            .frame(maxWidth: .infinity, maxHeight: 200)
            .clipped()
        } placeholder: {
          ProgressView()
            .frame(maxWidth: .infinity)
        }
        
        Group {
          Text(article.title)
            .font(.title)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top)
          Text(article.newsSite)
            .font(.title3)
            .fontWeight(.medium)
            .foregroundStyle(.secondary)
          Text(article.publishedAt.formatted(date: .abbreviated, time: .shortened))
            .font(.callout)
            .foregroundStyle(.secondary)
          Text(article.summary)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top)
        }
        .padding([.leading, .trailing])
        
        Spacer()
        Button {
          openURL(article.url)
          Analytics.logEvent("read_more", parameters: [
            "url": article.url
          ])
        } label: {
          HStack {
            Text("Read More...")
            Spacer()
            Image(systemName: "safari")
          }
        }
        .buttonStyle(.bordered)
        .controlSize(.large)
        .padding()
      }
      .onAppear {
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [
          AnalyticsParameterScreenName: "ArticleDetail",
          AnalyticsParameterScreenClass: "ArticleDetailView"
        ])
      }
    } else {
      Text("Select an article.")
        .foregroundStyle(.secondary)
    }
  }
}

struct ArticleDetailViewPreview: PreviewProvider {
  
  static var previews: some View {
    NavigationView {
      ArticleDetailView(article: .sample)
    }
  }
}
