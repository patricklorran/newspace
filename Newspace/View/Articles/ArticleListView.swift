//
//  ArticleListView.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 13/10/22.
//

import Firebase
import SwiftUI

struct ArticleListView: View {
  
  @ObservedObject
  private var viewModel = ArticleListViewModel()
  
  @State
  private var isPresentingInfo = false
  
  var body: some View {
    Group {
      switch viewModel.state {
        case .loading:
          ProgressView()
            .onAppear {
              viewModel.fetchArticles()
              Analytics.logEvent(AnalyticsEventScreenView, parameters: [
                AnalyticsParameterScreenName: "ArticleList",
                AnalyticsParameterScreenClass: "ArticleListView"
              ])
            }
        case .loaded(let articles):
          List(articles) { article in
            NavigationLink {
              ArticleDetailView(article: article)
            } label: {
              Text(article.title)
            }
          }
          .listStyle(.plain)
        case .error:
          ErrorView {
            viewModel.fetchArticles()
          }
      }
    }
    .navigationTitle("Articles")
    .toolbar {
      ToolbarItem {
        Button {
          isPresentingInfo = true
        } label: {
          Label("Info", systemImage: "info.circle")
        }
      }
    }
    .sheet(isPresented: $isPresentingInfo) {
      NavigationView {
        InfoView()
      }
    }
  }
}

struct ArticlesListPreviews: PreviewProvider {
  
  static var previews: some View {
    NavigationView {
      ArticleListView()
    }
  }
}
