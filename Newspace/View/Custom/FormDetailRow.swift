//
//  FormDetailRow.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 14/10/22.
//

import SwiftUI

struct FormDetailRow: View {
  
  var title: String
  var subtitle: String
  
  var axis: Axis = .horizontal
  
  var body: some View {
    switch axis {
      case .horizontal:
        HStack {
          Text(title)
          Spacer()
          Text(subtitle)
            .foregroundColor(.secondary)
            .textSelection(.enabled)
        }
      case .vertical:
        VStack(alignment: .leading, spacing: 4) {
          Text(title)
          Text(subtitle)
            .foregroundColor(.secondary)
            .textSelection(.enabled)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
  }
}

struct FormDetailRowPreviews: PreviewProvider {
  
  static var previews: some View {
    Group {
      FormDetailRow(title: "Title", subtitle: "Subtitle")
        .previewLayout(.fixed(width: 400, height: 44))
      FormDetailRow(title: "Title", subtitle: "Subtitle", axis: .vertical)
        .previewLayout(.fixed(width: 400, height: 60))
    }
    .padding()
  }
}
