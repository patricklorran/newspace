//
//  ErrorView.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 18/10/22.
//

import SwiftUI

struct ErrorView: View {
  
  var message = "An error ocurred.\nPlease try again later."
  var retryHandler: (() -> Void)?
  
  var body: some View {
    VStack(alignment: .center, spacing: 40) {
      Image(systemName: "exclamationmark.circle")
        .foregroundStyle(.secondary)
        .font(.system(size: 100, weight: .ultraLight, design: .default))
      Text(message)
        .foregroundStyle(.secondary)
        .multilineTextAlignment(.center)
      Button {
        retryHandler?()
      } label: {
        Label("Retry", systemImage: "arrow.clockwise")
      }
    }
  }
}

struct ErrorViewPreviews: PreviewProvider {
  
  static var previews: some View {
    ErrorView()
  }
}
