//
//  InfoView.swift
//  Newspace
//
//  Created by Patrick Almeida Azeredo on 14/10/22.
//

import Firebase
import SwiftUI

struct InfoView: View {
  
  @Environment(\.dismiss)
  private var dismiss
  
  private let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "Unavailable"
  private let build   = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "Unavailable"
  
  private let firebaseClientID = FirebaseApp.app()?.options.clientID ?? "Unavailable"
  private let googleAppID = FirebaseApp.app()?.options.googleAppID ?? "Unavailable"
  private let gcmSenderID = FirebaseApp.app()?.options.gcmSenderID ?? "Unavailable"
  
  var body: some View {
    Form {
      Section("Application") {
        FormDetailRow(title: "Version", subtitle: version)
        FormDetailRow(title: "Build", subtitle: build)
      }
      Section("Firebase") {
        FormDetailRow(title: "Client ID", subtitle: firebaseClientID, axis: .vertical)
        FormDetailRow(title: "Google App ID", subtitle: googleAppID, axis: .vertical)
        FormDetailRow(title: "GCM Sender ID", subtitle: gcmSenderID)
      }
    }
    .navigationTitle("Info")
    .toolbar {
      ToolbarItem {
        Button {
          dismiss()
        } label: {
          Label("Close", systemImage: "xmark.circle.fill")
            .tint(.secondary)
        }
      }
    }
  }
}

struct AboutViewPreviews: PreviewProvider {
  
  static var previews: some View {
    NavigationView {
      InfoView()
    }
  }
}
